import { useRouter } from 'next/router';
import React from 'react';

function ClientProjects() {
  const router = useRouter();
  function loadProjectHandler() {
    router.push('/clients/max/12');
  }
  return <div>
    <h1>List of client projects</h1>
    <button onClick={loadProjectHandler}>Load projects</button>
  </div>;
}

export default ClientProjects;

